/* This file explains the common data types in C++ which are :

int --> Stores whole numbers without decimals ex: 3,44 etc ----> 2=4 bytes
char -> Stores single character/letter/number or any ASCII values --> 1 bytes
float-> Stores numbers with decimals ===> 4 bytes
double->Stores numbers like float but with more precision ==> 8 bytes
string -> stores characters --> #include <string> */


#include <string>
#include <iostream>
using namespace std ; 

string a = "Abhishek is here";
int b = 24;
float c = 8.08;
double d = 3.1457283461 ; 

int main ()
{
	cout << "This is a string= ";
	cout << a+"\n";
	cout << "My age is = ";
	cout << b;
	cout << "\n"; 
	cout << "My CGPA is = ";
	cout << c,
	cout << "\n";
	cout << "the value of pi is = ";
	cout << d,
	cout << "\n";
	return 0 ;
}

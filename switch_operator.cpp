// this explains the use of switch operator 
/* switch takes an input 
eg - switch(input)	which is an integer 
switch(input)
{
it writes various cases with integer values based on our problem statement ( we can have n number of cases) 
it will execute the case that matches with the input number 
}
*/

#include <iostream>
using namespace std ;

int x = 5;
int main(){
	switch(x){
		case 1:
			cout << "Monday";
			break;
		case 2: 
			cout << "Tuesday";
			break;
		case 3:
			cout << "Wednesday";
			break;
		case 4:
			cout << "Thursday";
			break;
		case 5:
			cout << "Friday";
			break;
		case 6:
			cout << "Saturday";
			break;	
	}
	return 0;	
}
